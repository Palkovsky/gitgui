package pl.dawidmacek.listview;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.AnchorPane;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import pl.dawidmacek.git.GitWrapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CommitListViewCell extends ListCell<RevCommit> {

    @FXML
    private Label hashLabel;
    @FXML
    private Label messageLabel;
    @FXML
    private Label branchesLabel;
    @FXML
    private AnchorPane root;

    private GitWrapper git;
    private FXMLLoader loader;
    private Map<String, Set<RevCommit>> branchLookup;

    public CommitListViewCell(GitWrapper git) {
        this.git = git;
        branchLookup = new HashMap<>();
        for(Ref ref : git.getBranches()){
            String[] chunks = ref.getName().split("/");
            branchLookup.put(chunks[chunks.length-1], new HashSet<>(git.getCommitsByBranch(ref.getName())));
        }
    }

    @Override
    protected void updateItem(RevCommit commit, boolean empty) {
        super.updateItem(commit, empty);

        setText(null);
        if(empty || commit == null){
            setGraphic(null);
            return;
        }


        if (!empty && commit != null) {
            if(loader == null) {
                loader = new FXMLLoader(getClass().getResource("/layouts/items/commit_item.fxml"));
                loader.setController(this);
                try {
                    loader.load();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            final String[] branches = {""};
            branchLookup.forEach((key, set) -> {
                if(set.contains(commit)) branches[0] += key + " | ";
            });
            String branchesString = branches[0];
            if(branchesString.length() >= 3) branchesString = branchesString.substring(0, branchesString.length()-3);

            hashLabel.setText(commit.getId().getName());
            messageLabel.setText(commit.getFullMessage());
            branchesLabel.setText(branchesString);
            setGraphic(root);
        }
    }

}
