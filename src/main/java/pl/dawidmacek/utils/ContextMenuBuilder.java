package pl.dawidmacek.utils;

import javafx.scene.control.*;

import java.util.LinkedList;
import java.util.List;

public class ContextMenuBuilder {

    public interface OnClickListener {
        void onClick();
    }

    public interface OnCheckListener {
        void onCheck(boolean value);
    }

    private List<MenuItem> menuItemList;

    public ContextMenuBuilder() {
        menuItemList = new LinkedList<>();
    }

    public ContextMenuBuilder item(String label, OnClickListener callback){
        MenuItem menuItem = new MenuItem(label);
        if(callback != null) menuItem.setOnAction(value -> callback.onClick());
        menuItemList.add(menuItem);
        return this;
    }

    public ContextMenuBuilder separator(){
        menuItemList.add(new SeparatorMenuItem());
        return this;
    }

    public ContextMenuBuilder nested(String label, ContextMenuBuilder builder){
        Menu menu = new Menu(label);
        menu.getItems().addAll(builder.build().getItems());
        menuItemList.add(menu);
        return this;
    }

    public ContextMenuBuilder checkbox(String label, boolean def, OnCheckListener callback){
        CheckMenuItem checkMenuItem = new CheckMenuItem(label);
        checkMenuItem.setSelected(def);
        if(callback != null) checkMenuItem.setOnAction(value -> callback.onCheck(checkMenuItem.isSelected()));
        menuItemList.add(checkMenuItem);
        return this;
    }

    public ContextMenu build(){
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.getItems().addAll(menuItemList);
        return contextMenu;
    }
}
