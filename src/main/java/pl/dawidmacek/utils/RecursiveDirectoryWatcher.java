package pl.dawidmacek.utils;

import javafx.application.Platform;

import java.io.IOException;
import java.nio.file.*;
import java.util.stream.Collectors;

public class RecursiveDirectoryWatcher implements Runnable{

    public enum EventType{
        CREATED, UPDATED, DELETED
    }

    public interface Callback{
        void onEvent(EventType event, Path path);
    }

    private Path basePath;
    private WatchService watchService;
    private Callback callback;

    private RecursiveDirectoryWatcher(Path basePath, Callback callback) {
        this.basePath = basePath;
        this.callback = callback;
    }

    private void registerPath(Path path) throws IOException {
        for(Path dir : Files.walk(path).filter(p -> Files.isDirectory(p)).distinct().collect(Collectors.toList())){
            if(dir.toString().contains(".git")) continue;
            dir.register(watchService, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
        }
    }

    @Override
    public void run() {
        try {
            this.watchService = FileSystems.getDefault().newWatchService();
            registerPath(basePath);

            WatchKey key;
            long lastNotification = System.currentTimeMillis();
            while (true){

                while (System.currentTimeMillis() - lastNotification < 5000) continue;

                key = watchService.take();
                for(WatchEvent<?> event : key.pollEvents()){
                    WatchEvent.Kind<?> kind = event.kind();
                    if(kind == StandardWatchEventKinds.OVERFLOW) continue;

                    EventType type = EventType.CREATED;
                    if(kind == StandardWatchEventKinds.ENTRY_DELETE) type = EventType.DELETED;
                    else if(kind == StandardWatchEventKinds.ENTRY_MODIFY) type = EventType.UPDATED;

                    WatchEvent<Path> ev = (WatchEvent<Path>) event;
                    Path filename = ev.context();
                    EventType finalType = type;
                    lastNotification = System.currentTimeMillis();
                    if(callback != null) {
                        Platform.runLater(() -> {
                            callback.onEvent(finalType, filename);
                        });
                    }
                    break;
                }
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static RecursiveDirectoryWatcher build(Path path, Callback callback)  {
        return new RecursiveDirectoryWatcher(path, callback);
    }

    public static RecursiveDirectoryWatcher build(String path, Callback callback) {
        return RecursiveDirectoryWatcher.build(Paths.get(path), callback);
    }
}
