package pl.dawidmacek.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import pl.dawidmacek.controllers.AbstractController;
import pl.dawidmacek.controllers.MessageDialog;
import pl.dawidmacek.controllers.ProjectController;
import pl.dawidmacek.controllers.YesNoDialog;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.git.GitWrapper;

import java.io.File;
import java.io.IOException;

public final class StageSpawner {

    private StageSpawner() {
    }

    public static final String SETTINGS = "layouts/project_settings/project_settings.fxml";
    public static final String PROJECT = "layouts/project/project.fxml";
    public static final String FILE_LISTING = "layouts/project/file_listing.fxml";
    public static final String PROJECT_STATUS = "layouts/project/status.fxml";
    public static final String PROJECT_STAGED_INFO = "layouts/project/staged_info.fxml";
    public static final String PERFORM_COMMIT_DIALOG = "layouts/commit_dialog.fxml";
    public static final String COMMIT_HISTORY_DIALOG = "layouts/commit_history.fxml";
    public static final String MERGE_DIALOG = "layouts/merge_dialog.fxml";
    public static final String CREATE_BRANCH_DIALOG = "layouts/new_branch_dialog.fxml";
    public static final String MESSAGE_DIALOG = "layouts/message_dialog.fxml";
    public static final String YES_NO_DIALOG = "layouts/yes_no_dialog.fxml";

    public static Parent loadFXMLWithController(String path, AbstractController controller) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setController(controller);
        try {
            return fxmlLoader.load(StageSpawner.class.getClassLoader().getResource(path).openStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Stage spawnModalDialog(String fxmlPath, String title, Stage owner) {
        try {
            Parent root = FXMLLoader.load(StageSpawner.class.getClassLoader().getResource(fxmlPath));
            return StageSpawner.spawnModalDialog(root, title, owner);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Stage spawnMessageDialog(String title, String message, Stage owner) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            Parent root = fxmlLoader.load(StageSpawner.class.getClassLoader().getResource(MESSAGE_DIALOG).openStream());
            MessageDialog dialog = fxmlLoader.getController();
            dialog.setMessage(message);
            return StageSpawner.spawnModalDialog(root, title, owner);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Stage spawnYesNoDialog(String title, String message, Stage owner, YesNoDialog.OnYesCallback onYes, YesNoDialog.OnNoCallback onNo) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        try {
            Parent root = fxmlLoader.load(StageSpawner.class.getClassLoader().getResource(YES_NO_DIALOG).openStream());
            YesNoDialog dialog = fxmlLoader.getController();
            dialog.setMessage(message);
            dialog.setOnYesListener(onYes);
            dialog.setOnNoListener(onNo);
            return StageSpawner.spawnModalDialog(root, title, owner);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Stage spawnErrorDialog(Exception e, Stage owner) {
        return StageSpawner.spawnMessageDialog("Error", e.getMessage(), owner);
    }

    //Performs whole logic responsible for opening new git repository
    public static GitWrapper spawnOpenProjectDialog(Stage stage) {
        DirectoryChooser chooser = new DirectoryChooser();
        chooser.setTitle("Open git repository");

        File selectedDirectory = chooser.showDialog(stage);
        if (selectedDirectory == null) return null;

        GitWrapper git = null;
        if (GitUtils.isGitDirectory(selectedDirectory)) {
            try {
                git = GitWrapper.wrap(GitUtils.openRepository(selectedDirectory));
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Repository[] finalRepository = new Repository[1];
            StageSpawner.spawnYesNoDialog(
                    "Question",
                    "No git repo detected in this directory. Create one?",
                    stage,
                    () -> {
                        try {
                            finalRepository[0] = GitUtils.initRepository(selectedDirectory);
                        } catch (GitAPIException e) {
                            e.printStackTrace();
                        }
                    },
                    null);

            if (finalRepository[0] == null) return null;
            git = GitWrapper.wrap(finalRepository[0]);

        }

        //We automatically perform initial commit just to ensure there's a HEAD
        if (git != null && git.getGit().getRepository().getAllRefs().isEmpty()) {
            try {
                GitUtils.commit(git.getGit(), "HEAD commit");
            } catch (GitAPIException e) {
                e.printStackTrace();
            }
        }

        return git;
    }


    public static Stage spawnProjectStage(GitWrapper git) {
        Parent root = StageSpawner.loadFXMLWithController(PROJECT, new ProjectController(git));
        Stage stage = new Stage();
        stage.setTitle(git.getGit().getRepository().getDirectory().getAbsolutePath());
        stage.setScene(new Scene(root));
        stage.setMinHeight(600);
        stage.setMinWidth(800);
        stage.show();
        return stage;
    }

    public static Stage spawnModalDialog(Parent root, String title, Stage owner) {
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.initOwner(owner);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();
        return stage;
    }
}
