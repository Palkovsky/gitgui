package pl.dawidmacek.utils;

import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;


public final class Layouts {

    private Layouts() {
    }

    public static Node emptyListPlaceholder(String text, String imageFile){
        VBox box = new VBox();
        box.setFillWidth(true);
        box.setAlignment(Pos.CENTER);
        if(imageFile != null){
            ImageView imageView = new ImageView(new Image(imageFile));
            box.getChildren().add(imageView);
        }
        Label label = new Label(text);
        label.setFont(Font.font("Arial", 18));
        box.getChildren().add(label);
        return box;
    }

}
