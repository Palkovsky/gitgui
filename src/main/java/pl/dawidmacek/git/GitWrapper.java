package pl.dawidmacek.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import pl.dawidmacek.datastructs.GitFile;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class GitWrapper implements AutoCloseable {

    private Repository repository;
    private Git git;

    private GitWrapper(Repository repository) {
        this.repository = repository;
        this.git = new Git(this.repository);
    }

    public static GitWrapper wrap(Repository repository) {
        return new GitWrapper(repository);
    }

    public Git getGit() {
        return git;
    }

    public Repository getRepository() {
        return getGit().getRepository();
    }

    public File getProjectPath() {
        File gitPath = getRepository().getDirectory();
        return gitPath.getParentFile();
    }

    @Override
    public void close() {
        repository.close();
    }

    public String getFilepattern(GitFile gitFile) {
        return getProjectPath().toPath().relativize(gitFile.getFile().toPath()).toString().replaceAll("\\\\", "/");
    }

    public String getCurrentBranch() {
        try {
            return repository.getBranch();
        } catch (IOException e) {
            return "---";
        }
    }

    public List<RevCommit> getAllCommits() {
        List<RevCommit> commitList = new LinkedList<>();
        try {
            Iterable<RevCommit> commits = git.log().all().call();
            for (RevCommit commit : commits) commitList.add(commit);
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        }
        return commitList;
    }

    public List<RevCommit> getCommitsOnCurrentBranch() {
        return getCommitsByBranch("refs/heads/" + getCurrentBranch());
    }

    public List<RevCommit> getCommitsByBranch(String treeName) {
        List<RevCommit> commitList = new LinkedList<>();
        try {
            for (RevCommit commit : git.log().add(repository.resolve(treeName)).call()) commitList.add(commit);
        } catch (IOException | GitAPIException e) {
            e.printStackTrace();
        }
        return commitList;
    }

    public List<Ref> getBranches() {
        List<Ref> branchesList = new LinkedList<>();
        try {
            branchesList.addAll(git.branchList().call());
        } catch (GitAPIException e) {
            e.printStackTrace();
        }
        return branchesList;
    }
}
