package pl.dawidmacek.git;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import pl.dawidmacek.datastructs.GitFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public final class GitUtils {

    private GitUtils() {
    }

    public static boolean isGitDirectory(File path) {
        Repository repository = null;
        File gitDir = Paths.get(path.getAbsolutePath()).resolve(".git").toFile();
        return gitDir.exists();
    }

    public static Repository openRepository(File path) throws IOException {
        File gitDir = Paths.get(path.getAbsolutePath()).resolve(".git").toFile();
        FileRepositoryBuilder builder = new FileRepositoryBuilder();
        builder.setMustExist(true);
        Repository repository =
                builder.setGitDir(gitDir)
                        .readEnvironment()
                        .findGitDir()
                        .build();
        return repository;
    }

    public static Repository initRepository(File path) throws GitAPIException {
        return Git.init().setDirectory(path).call().getRepository();
    }

    public static Repository openOrCreateRepository(File path) throws GitAPIException, IOException {
        if (isGitDirectory(path)) return GitUtils.openRepository(path);
        else return GitUtils.initRepository(path);
    }

    public static Set<String> getHeadFiles(Repository repository) throws IOException {
        Ref head = repository.getRef("HEAD");

        // a RevWalk allows to walk over commits based on some filtering that is defined
        RevWalk walk = new RevWalk(repository);

        RevCommit commit = null;
        try {
            commit = walk.parseCommit(head.getObjectId());
        } catch (NullPointerException e) { //jgit throw NPE if repo has no commits, naughty library
            return new HashSet<>();
        }

        RevTree tree = commit.getTree();

        // now use a TreeWalk to iterate over all files in the Tree recursively
        // you can set Filters to narrow down the results if needed
        Set<String> results = new HashSet<>();
        TreeWalk treeWalk = new TreeWalk(repository);
        treeWalk.addTree(tree);
        treeWalk.setRecursive(false);
        while (treeWalk.next()) {
            if (treeWalk.isSubtree()) {
                treeWalk.enterSubtree();
            } else {
                results.add(treeWalk.getPathString());
            }
        }
        return results;
    }


    public static RevCommit commit(Git git, String message) throws GitAPIException {
        return git.commit().setMessage(message).call();
    }

    public static Ref unstageToHead(Git git, ResetCommand.ResetType resetType) throws GitAPIException {
        return git.reset().setMode(resetType).call();
    }

    public static void stageGitFile(GitWrapper git, GitFile gitFile) throws GitAPIException {
        switch (gitFile.getStatus()) {
            case REMOVED:
                git.getGit().rm().addFilepattern(git.getFilepattern(gitFile)).call();
                break;
            default:
                git.getGit().add().addFilepattern(git.getFilepattern(gitFile)).call();
                break;
        }
    }

    public static void stageManyGitFiles(GitWrapper git, Collection<GitFile> files) throws GitAPIException {
        for (GitFile file : files) GitUtils.stageGitFile(git, file);
    }

    public static void unstageGitFile(GitWrapper git, GitFile gitFile) throws GitAPIException {
        git.getGit().reset().setRef("HEAD").addPath(git.getFilepattern(gitFile)).call();
    }


    public static void unstageManyGitFiles(GitWrapper git, Collection<GitFile> files) throws GitAPIException {
        for (GitFile file : files) GitUtils.unstageGitFile(git, file);
    }

    public static void removeGitFile(GitWrapper git, GitFile gitFile) throws GitAPIException {
        git.getGit().rm().setCached(true).addFilepattern(git.getFilepattern(gitFile)).call();
    }

    public static void removeManyGitFiles(GitWrapper git, Collection<GitFile> files) throws GitAPIException {
        for (GitFile file : files) GitUtils.removeGitFile(git, file);
    }


    public static void removeByFilepattern(GitWrapper git, String pattern) throws GitAPIException {
        git.getGit().rm().setCached(true).addFilepattern(pattern).call();
    }

    public static void removeManyByFilepatterns(GitWrapper git, Collection<String> filepatterns) throws GitAPIException {
        for (String pattern : filepatterns) GitUtils.removeByFilepattern(git, pattern);
    }

    public static void addByFilepattern(GitWrapper git, String pattern) throws GitAPIException {
        git.getGit().add().addFilepattern(pattern).call();
    }

    public static void addManyByFilepatterns(GitWrapper git, Collection<String> filepatterns) throws GitAPIException {
        for (String pattern : filepatterns) GitUtils.addByFilepattern(git, pattern);
    }
}
