package pl.dawidmacek.datastructs;

import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.StageSpawner;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GitFile{

    public enum Status{
        DIRECTORY(Color.valueOf("#000000")),
        CURRENT(Color.valueOf("#000000")), //up to date with current branch
        ADDED(Color.valueOf("#388E3C")),
        CHANGED(Color.valueOf("#0277BD")),
        REMOVED(Color.valueOf("#FF0000")),
        UNTRACKED(Color.valueOf("#C62828"));


        private Color color;

        Status(Color color) {
            this.color = color;
        }

        public Color getColor() {
            return color;
        }
    }

    private File file;
    private Status status;

    public GitFile(File file, Status status) {
        this.file = file;
        this.status = status;
    }

    public void open(Stage parent){
        try {
            Desktop.getDesktop().open(getFile());
        } catch (IOException e) {
            StageSpawner.spawnModalDialog("Error", "Unable to open " + file.getName(), parent);
        }
    }

    public GitFile(File file) {
        this(file, Status.UNTRACKED);
    }

    public boolean isDirectory(){
        return file.isDirectory();
    }

    public boolean isFile(){
        return file.isFile();
    }

    public String getName() {
        return file.getName();
    }

    public Status getStatus() {
        return status;
    }


    public void setStatus(Status status) {
        this.status = status;
    }

    public File getFile() {
        return file;
    }

    public boolean isStaged(GitWrapper git){
       return true;
    }
}
