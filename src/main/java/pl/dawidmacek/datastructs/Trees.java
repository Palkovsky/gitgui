package pl.dawidmacek.datastructs;

import javafx.scene.control.TreeItem;

import java.nio.file.Path;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class Trees {
    private Trees() {
    }

    /*
        Converts DirectoryNode tree to TreeItem for use in TableTreeView.
     */
    public static <E> TreeItem<E> convertToTreeItem(DirectoryNode<E> root){
        TreeItem<E> treeItem = new TreeItem<E>(root.getValue());
        for(DirectoryNode<E> child : root.getChildren()){
            treeItem.getChildren().add(Trees.convertToTreeItem(child));
        }
        return treeItem;
    }


    /*
        If there are null nodes in GitFile tree, we assume it is directory.
     */
    public static void normalizeTree(Path basePath, DirectoryNode<GitFile> root){
        if(root.getValue() == null) {
            root.setValue(new GitFile(basePath.toFile(), GitFile.Status.DIRECTORY));
        }
        for(DirectoryNode<GitFile> node : root.getChildren()) Trees.normalizeTree(basePath.resolve(node.getName()), node);
    }

    /*
        Flattens list of TreeItem<E> nodes to single set. Used for getting selected items recursively.
     */
    public static <E> Set<E> flatten(List<TreeItem<E>> nodes){
        Set<E> results = new HashSet<>();
        for(TreeItem<E> item : nodes){
            if(item == null) continue;
            results.add(item.getValue());
            results.addAll(Trees.flatten(item.getChildren()));
        }
        return results;
    }
}
