package pl.dawidmacek.datastructs;

import java.util.ArrayList;
import java.util.List;

/*
    Helper class. It allows to convert string paths into tree structure.
 */
public class DirectoryNode<E> {

    private String name;
    private E value;
    private List<DirectoryNode<E>> children;
    private DirectoryNode<E> parent;

    public DirectoryNode(String name, E value, DirectoryNode<E> parent) {
        this.name = name;
        this.parent = parent;
        this.value = value;
        this.children = new ArrayList<DirectoryNode<E>>();
    }

    public DirectoryNode(String name) {
        this(name, null, null);
    }

    public void addElement(String elementValue, E value) {
        String[] chunks = elementValue.split("/");

        DirectoryNode<E> node = this;
        for(int i=0; i<chunks.length; i++){
            String chunk = chunks[i];
            DirectoryNode<E> next = null;

            // Look for child to follow
            for(DirectoryNode<E> child : node.getChildren()){
                if(child.getName().equals(chunk)) {
                    next = child;
                    break;
                }
            }
            
            if(next == null) { //If there's none - create one
                next = new DirectoryNode<E>(chunk, (i==chunks.length - 1) ? value : null, node);
                node.getChildren().add(next);
            }

            node = next;
        }
    }

    public void printTree(){
        printTree(0);
    }

    private void printTree(int spaceOffset){
        String spaces = new String(new char[spaceOffset]).replace('\0', ' ');
        System.out.println(spaces + getName());
        for(DirectoryNode<E> child : getChildren()) child.printTree(spaceOffset + 2);
    }

    public DirectoryNode<E> getRoot() {
        DirectoryNode<E> node = this;
        while (node.getParent() != null) node = node.getParent();
        return node;
    }

    public String getName() {
        return name;
    }

    public DirectoryNode<E> getParent() {
        return parent;
    }

    public E getValue() {
        return value;
    }

    public void setValue(E value) {
        this.value = value;
    }

    public List<DirectoryNode<E>> getChildren() {
        return children;
    }

    public boolean isRoot(){
        return getParent() == null;
    }

    public boolean isLeaf(){
        return getChildren().size() == 0;
    }
}