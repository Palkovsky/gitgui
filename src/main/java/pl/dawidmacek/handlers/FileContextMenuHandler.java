package pl.dawidmacek.handlers;

import javafx.scene.control.ContextMenu;
import org.eclipse.jgit.api.errors.GitAPIException;
import pl.dawidmacek.controllers.ProjectController;
import pl.dawidmacek.datastructs.GitFile;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.utils.ContextMenuBuilder;
import pl.dawidmacek.utils.StageSpawner;

import java.util.Collection;

public final class FileContextMenuHandler {

    public ContextMenu handle(ProjectController controller, Collection<GitFile> selectedItems, Collection<String> selectedFilepatterns){
        if(selectedItems.size() == 1 && selectedFilepatterns.size() == 1) return singeItemMenu(controller, selectedItems.iterator().next());
        return multiItemMenu(controller, selectedFilepatterns);
    }

    private ContextMenu singeItemMenu(ProjectController controller, GitFile item){
        return new ContextMenuBuilder()
                .item("Info", ()-> StageSpawner.spawnMessageDialog(item.getName(), "Miejsce na interesujące informacje o: " + item.getFile().toString(), controller.getStage()))
                .item("Open in explorer", () -> item.open(controller.getStage()))
                .separator()
                .item("Add", ()-> {
                    try {
                        GitUtils.stageGitFile(controller.getGit(), item);
                        controller.reload();
                    } catch (GitAPIException e) {
                        StageSpawner.spawnErrorDialog(e, controller.getStage());
                    }
                })
                .item("Mark untracked", () -> {
                    try {
                        GitUtils.removeGitFile(controller.getGit(), item);
                        controller.reload();
                    } catch (GitAPIException e) {
                        StageSpawner.spawnErrorDialog(e, controller.getStage());
                    }
                })
                .separator()
                .checkbox("Kluski", true, null)
                .build();
    }

    private ContextMenu multiItemMenu(ProjectController controller, Collection<String> filepatterns){
       return new ContextMenuBuilder()
                .item("Add", () -> {
                    try {
                        GitUtils.addManyByFilepatterns(controller.getGit(), filepatterns);
                        controller.reload();
                    } catch (GitAPIException e) {
                        StageSpawner.spawnErrorDialog(e, controller.getStage());
                    }
                })
                .item("Mark untracked",  () -> {
                    try {
                        GitUtils.removeManyByFilepatterns(controller.getGit(), filepatterns);
                        controller.reload();
                    } catch (GitAPIException e) {
                        StageSpawner.spawnErrorDialog(e, controller.getStage());
                    }
                })
                .build();
    }
}
