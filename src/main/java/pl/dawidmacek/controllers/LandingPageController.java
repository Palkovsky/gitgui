package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.stage.DirectoryChooser;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Repository;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.StageSpawner;
import pl.dawidmacek.git.GitUtils;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LandingPageController extends AbstractController implements Initializable {

    @FXML
    private AnchorPane openBtn;
    @FXML
    private AnchorPane cloneBtn;
    @FXML
    private AnchorPane settingsBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        openBtn.setOnMouseClicked(e -> onOpenClick());
        cloneBtn.setOnMouseClicked(e -> onCloneClick());
        settingsBtn.setOnMouseClicked(e -> onSettingsClick());
    }

    private void onOpenClick() {
        GitWrapper git = StageSpawner.spawnOpenProjectDialog(getStage());
        if (git != null) {
            StageSpawner.spawnProjectStage(git);
            getStage().close();
        }
    }

    private void onCloneClick() {
        StageSpawner.spawnMessageDialog("Alert", "Feature not implemented yet.", getStage());
    }

    private void onSettingsClick() {
        StageSpawner.spawnModalDialog(StageSpawner.SETTINGS, "Settings", getStage());
    }


}
