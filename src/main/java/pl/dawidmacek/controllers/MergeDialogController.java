package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.eclipse.jgit.api.MergeCommand;
import org.eclipse.jgit.api.MergeResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.StageSpawner;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class MergeDialogController extends AbstractController implements Initializable {


    @FXML
    private ComboBox<String> sourceComboBox;
    @FXML
    private ComboBox<String> targetComboBox;
    @FXML
    private CheckBox squashCheckBox;
    @FXML
    private CheckBox sourceBranchRemoveCheckBox;
    @FXML
    private Button backBtn;
    @FXML
    private Button mergeBtn;

    private GitWrapper git;

    public MergeDialogController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> branches = git.getBranches().stream().map(ref -> {
            String[] chunks = ref.getName().split("/");
            return chunks[chunks.length - 1];
        }).collect(Collectors.toList());

        targetComboBox.getItems().addAll(branches);
        targetComboBox.getSelectionModel().select(git.getCurrentBranch());
        targetComboBox.setDisable(true);

        sourceComboBox.getItems().addAll(branches.stream().filter(branch -> !branch.equals(git.getCurrentBranch())).collect(Collectors.toList()));

        backBtn.setOnMouseClicked(e -> getStage().close());
        mergeBtn.setOnMouseClicked(e -> {
            //String targetBranch = targetComboBox.getSelectionModel().getSelectedItem();
            String sourceBranch = sourceComboBox.getSelectionModel().getSelectedItem();

            if (sourceBranch == null) {
                sourceComboBox.requestFocus();
                sourceComboBox.setBorder(new Border(new BorderStroke(Color.RED,
                        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                return;
            }

            try {
                ObjectId mergeBase = git.getRepository().resolve(sourceBranch);

                MergeCommand command = git.getGit().merge().include(mergeBase);
                if(squashCheckBox.isSelected()){
                    command.setSquash(true);
                } else {
                    command.setSquash(false).setFastForward(MergeCommand.FastForwardMode.NO_FF);
                }

                MergeResult merge = command.call();
                getStage().close();
                StageSpawner.spawnMessageDialog("Merge summary", merge.getMergeStatus().toString(), getStage());
                if(merge.getMergeStatus().isSuccessful() && sourceBranchRemoveCheckBox.isSelected()){
                    git.getGit().branchDelete().setBranchNames(sourceBranch).call();
                }

            } catch (IOException | GitAPIException e1) {
                StageSpawner.spawnErrorDialog(e1, getStage());
            }

        });
    }
}
