package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.ContextMenuBuilder;
import pl.dawidmacek.utils.StageSpawner;

import java.net.URL;
import java.util.ResourceBundle;

public class StatusController extends AbstractController implements Initializable {

    public interface StateChangeListener {
        void onStateChange();
    }

    public interface OnCommitHistoryRequest{
        void openCommitHistory();
    }

    @FXML
    private Label itemsSelectedLabel;
    @FXML
    private Label branchSelectLabel;
    @FXML
    private Label commitsCountLabel;

    private GitWrapper git;
    private OnCommitHistoryRequest onCommitHistoryRequestListener;
    private StateChangeListener stateChangeListener;

    public StatusController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        itemsSelectedLabel.setText("Ready!");

        branchSelectLabel.setOnMouseClicked(e -> {
            ContextMenuBuilder builder = new ContextMenuBuilder();
            for(Ref ref : git.getBranches()){
                String[] chunks = ref.getName().split("/");
                String shortBranchName = chunks[chunks.length-1];
                builder.checkbox(shortBranchName, shortBranchName.equals(git.getCurrentBranch()), (boolean checked) -> {
                    if(!checked) return; // same branch
                    StageSpawner.spawnYesNoDialog("Checkout", "Do you wish to checkout to '" + shortBranchName + "'?", getStage(),
                            () ->{
                                try {
                                    git.getGit().checkout().setName(ref.getName()).call();
                                    if(stateChangeListener != null) stateChangeListener.onStateChange();
                                } catch (GitAPIException e1) {
                                    StageSpawner.spawnErrorDialog(e1, getStage());
                                }
                            }, null);
                });
            }

            // Adding new branch from context menu
            builder.separator();
            builder.item("Branch out", ()-> {
                NewBranchDialogController.OnSubmitListener callback = (branchName, checkout) -> {
                    try {
                        Ref branch = git.getGit().branchCreate().setName(branchName).call();
                        if(checkout){
                            git.getGit().checkout().setForce(true).setName(branch.getName()).call();
                            if(stateChangeListener != null) stateChangeListener.onStateChange();
                        }
                    } catch (GitAPIException e1) {
                        StageSpawner.spawnErrorDialog(e1, getStage());
                    }
                };
                StageSpawner.spawnModalDialog(
                        StageSpawner.loadFXMLWithController(
                                StageSpawner.CREATE_BRANCH_DIALOG,
                                new NewBranchDialogController(callback)),
                        git.getProjectPath().toString() + " - New branch", getStage());
            });

            builder.build().show(getStage(), e.getScreenX(), e.getScreenY());
        });

        commitsCountLabel.setOnMouseClicked(e -> {
            if(onCommitHistoryRequestListener != null) onCommitHistoryRequestListener.openCommitHistory();
        });

        reload();
    }


    public void setStatusMessage(String message){
        itemsSelectedLabel.setText(message);
    }

    public void reload(){
        branchSelectLabel.setText(git.getCurrentBranch());

        int commitsCount = git.getCommitsOnCurrentBranch().size();
        String commitsCountMessage = "No commits";
        if(commitsCount == 1) commitsCountMessage = "1 commit";
        else if(commitsCount > 1) commitsCountMessage = commitsCount + " commits";
        commitsCountLabel.setText(commitsCountMessage);
    }

    public void setOnCommitHistoryRequestListener(OnCommitHistoryRequest onCommitHistoryRequestListener) {
        this.onCommitHistoryRequestListener = onCommitHistoryRequestListener;
    }

    public void setOnStateChangeListener(StateChangeListener stateChangeListener) {
        this.stateChangeListener = stateChangeListener;
    }
}