package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import org.eclipse.jgit.revwalk.RevCommit;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.listview.CommitListViewCell;
import pl.dawidmacek.utils.NoSelectionModel;

import java.net.URL;
import java.util.*;

public class CommitHistoryController extends AbstractController implements Initializable {

    @FXML
    private TextField searchTextField;
    @FXML
    private ListView<RevCommit> commitsListView;

    private GitWrapper git;

    public CommitHistoryController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        searchTextField.textProperty().addListener(((observable, oldValue, newValue) -> {
            System.out.println(newValue);
        }));
        commitsListView.setSelectionModel(new NoSelectionModel<>());
        commitsListView.setCellFactory(lv -> new CommitListViewCell(git));
        commitsListView.getItems().addAll(git.getCommitsOnCurrentBranch());
        /*
        commitsListView.setOnMouseClicked(v -> {
            RevCommit selected = commitsListView.getSelectionModel().getSelectedItem();
            System.out.println("Selected: " + selected.getId().getName());
        });
        */
    }
}
