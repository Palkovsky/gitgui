package pl.dawidmacek.controllers;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import pl.dawidmacek.datastructs.GitFile;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.Layouts;
import pl.dawidmacek.utils.StageSpawner;

import java.net.URL;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class StagedInfoController extends AbstractController implements Initializable {

    public interface StateChangeListener {
        void onStateChange();
    }

    private GitWrapper git;

    @FXML
    private ListView<GitFile> stagedFilesListView;
    @FXML
    private ListView<GitFile> unstagedFilesListView;
    @FXML
    private AnchorPane stageAllBtn;
    @FXML
    private AnchorPane unstageAllBtn;

    private StateChangeListener stateChangeListener;

    public StagedInfoController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        setUpListView(stagedFilesListView, ev -> {
            GitFile gitFile = stagedFilesListView.getSelectionModel().getSelectedItem();
            if(gitFile != null && ev.getClickCount() == 2 && ev.getButton() == MouseButton.PRIMARY){
                try {
                    GitUtils.unstageGitFile(git, gitFile);
                    if(stateChangeListener != null) stateChangeListener.onStateChange();
                } catch (GitAPIException e) {
                    StageSpawner.spawnErrorDialog(e, getStage());
                }
            }
        });
        stagedFilesListView.setPlaceholder(Layouts.emptyListPlaceholder("No staged changes", "icons/staged-empty-48.png"));

        setUpListView(unstagedFilesListView, ev -> {
            GitFile gitFile = unstagedFilesListView.getSelectionModel().getSelectedItem();
            if(gitFile != null && ev.getClickCount() == 2 && ev.getButton() == MouseButton.PRIMARY){
                try {
                    GitUtils.stageGitFile(git, gitFile);
                    if(stateChangeListener != null) stateChangeListener.onStateChange();
                } catch (GitAPIException e) {
                    StageSpawner.spawnErrorDialog(e, getStage());
                }
            }
        });
        unstagedFilesListView.setPlaceholder(Layouts.emptyListPlaceholder("No unstaged changes", "icons/unstaged-empty-48.png"));

        // Buttons set up
        Tooltip.install(stageAllBtn, new Tooltip("Stage all"));
        Tooltip.install(unstageAllBtn, new Tooltip("Unstage all"));
        stageAllBtn.setOnMouseClicked(e -> {
            try {
                GitUtils.stageManyGitFiles(git, getUnstaged());
                if(stateChangeListener != null) stateChangeListener.onStateChange();
            } catch (GitAPIException e1) {
                e1.printStackTrace();
            }
        });
        unstageAllBtn.setOnMouseClicked(e -> {
            try {
                GitUtils.unstageManyGitFiles(git, getStaged());
                if(stateChangeListener != null) stateChangeListener.onStateChange();
            } catch (GitAPIException e1) {
                e1.printStackTrace();
            }
        });

        reload();
    }

    private void setUpListView(ListView<GitFile> lv, EventHandler<MouseEvent> callback){
        lv.setCellFactory(value -> new ListCell<GitFile>(){
            @Override
            protected void updateItem(GitFile item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : git.getFilepattern(item));
                setTextFill(empty ? null : item.getStatus().getColor());
            }
        });
        lv.setOnMouseClicked(callback);

    }


    public void reload(){
        stagedFilesListView.getItems().clear();
        unstagedFilesListView.getItems().clear();

        try {
            Path basePath = git.getProjectPath().toPath();
            Status status = git.getGit().status().call();

            Set<GitFile> unstaged = new HashSet<>();
            unstaged.addAll(status.getModified().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.CHANGED)).collect(Collectors.toSet()));
            unstaged.addAll(status.getMissing().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.REMOVED)).collect(Collectors.toSet()));
            //unstaged.addAll(status.getUntracked().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.UNTRACKED)).collect(Collectors.toSet()));

            unstagedFilesListView.getItems().addAll(unstaged);

            Set<GitFile> staged = new HashSet<>();
            staged.addAll(status.getChanged().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.CHANGED)).collect(Collectors.toSet()));
            staged.addAll(status.getRemoved().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.REMOVED)).collect(Collectors.toSet()));
            staged.addAll(status.getAdded().stream().map(e -> new GitFile(basePath.resolve(e).toFile(), GitFile.Status.ADDED)).collect(Collectors.toSet()));

            stagedFilesListView.getItems().addAll(staged);

            stagedFilesListView.getItems().sort(Comparator.comparing(f -> f.getFile().getAbsolutePath()));
            unstagedFilesListView.getItems().sort(Comparator.comparing(f -> f.getFile().getAbsolutePath()));

        } catch (GitAPIException e) {
            e.printStackTrace();
        }

        // Enable/disable buttons based on staged/unstaged files.
        unstageAllBtn.setDisable(stagedFilesListView.getItems().isEmpty());
        stageAllBtn.setDisable(unstagedFilesListView.getItems().isEmpty());
    }

    public List<GitFile> getStaged(){
        return stagedFilesListView.getItems();
    }

    public List<GitFile> getUnstaged(){
        return unstagedFilesListView.getItems();
    }

    public void setOnStateChangeListener(StateChangeListener stateChangeListener) {
        this.stateChangeListener = stateChangeListener;
    }
}
