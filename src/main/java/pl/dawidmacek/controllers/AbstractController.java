package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.stage.Stage;

public class AbstractController {

    @FXML
    private Parent root;

    public Stage getStage() {
        return (Stage) root.getScene().getWindow();
    }

}
