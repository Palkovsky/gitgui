package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

import java.net.URL;
import java.util.ResourceBundle;

public class AboutController extends AbstractController implements Initializable {

    @FXML
    private Button okBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        okBtn.setOnMouseClicked(e -> handleOkClick());
    }

    private void handleOkClick() {
        getStage().close();
    }
}
