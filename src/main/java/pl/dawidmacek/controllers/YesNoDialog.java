package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public final class YesNoDialog extends AboutController implements Initializable {

    public interface OnYesCallback{
        void onYes();
    }

    public interface OnNoCallback{
        void onNo();
    }

    private OnYesCallback onYesListener;
    private OnNoCallback onNoListener;

    @FXML
    private Label messageLabel;
    @FXML
    private Button yesBtn;
    @FXML
    private Button noBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        yesBtn.setOnMouseClicked(e -> {
            getStage().close();
            if (onYesListener != null) onYesListener.onYes();
        });
        noBtn.setOnMouseClicked(e -> {
            getStage().close();
            if (onNoListener != null) onNoListener.onNo();
        });
    }

    public void setMessage(String message) {
        if(getLabel() != null) getLabel().setText(message);
    }

    public Label getLabel() {
        return messageLabel;
    }

    public void setOnYesListener(OnYesCallback onYesListener) {
        this.onYesListener = onYesListener;
    }

    public void setOnNoListener(OnNoCallback onNoListener) {
        this.onNoListener = onNoListener;
    }
}
