package pl.dawidmacek.controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

public class ProjectSettingsController implements Initializable {

    private Map<String, String> layoutMap;

    @FXML
    private ListView<String> optionsListView;
    @FXML
    private AnchorPane paneContent;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        layoutMap = new HashMap<>();
        layoutMap.put("Theme", "layouts/project_settings/theme.fxml");
        layoutMap.put("About", "layouts/project_settings/about.fxml");

        optionsListView.getItems().addAll(layoutMap.keySet());
        optionsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                System.out.println(newValue);
                handleItemSelected(newValue);
            }
        });
        optionsListView.setOnEditStart(e -> handleItemSelected(e.getNewValue()));
        optionsListView.getSelectionModel().select(0);
    }

    private void handleItemSelected(String val){
        try {
            AnchorPane pane = FXMLLoader.load(getClass().getClassLoader().getResource(layoutMap.get(val)));
            paneContent.getChildren().clear();
            paneContent.getChildren().setAll(pane);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
