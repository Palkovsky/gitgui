package pl.dawidmacek.controllers;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import org.eclipse.jgit.api.Status;
import org.eclipse.jgit.api.errors.GitAPIException;
import pl.dawidmacek.datastructs.DirectoryNode;
import pl.dawidmacek.datastructs.Trees;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.datastructs.GitFile;
import pl.dawidmacek.utils.Layouts;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;


public class FileListingController extends AbstractController implements Initializable {

    public interface OnItemsSelectCallback {
        void onSelectedItem(Set<GitFile> selected);
    }

    public interface OnItemClickCallback {
        void onItemClick(GitFile file);
    }

    public interface OnItemContextMenuOpen {
        void onContextMenu(double x, double y);
    }

    private GitWrapper git;
    private OnItemsSelectCallback onItemsSelectCallback;
    private OnItemClickCallback onItemClickCallback;
    private OnItemContextMenuOpen onItemContextMenuOpen;

    @FXML
    private TreeTableView<GitFile> treeView;

    public FileListingController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {


        // Creating custom cell factory for tree view
        treeView.setShowRoot(false);
        treeView.setPlaceholder(Layouts.emptyListPlaceholder("It's empty here", "icons/empty-files-48.png"));
        reload();
        treeView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        treeView.getSelectionModel().selectedItemProperty().addListener(((observable, oldValue, newValue) -> {
            if(onItemsSelectCallback != null) onItemsSelectCallback.onSelectedItem(getSelectedItems());
        }));
        treeView.sortPolicyProperty().set(t -> {
            Comparator<TreeItem<GitFile>> comparator = (t1, t2) -> {
                GitFile f1 = t1.getValue();
                GitFile f2 = t2.getValue();
                if(f1.isDirectory() && f2.isDirectory()) return f1.getName().toLowerCase().compareTo(f2.getName().toLowerCase());
                if(f1.isDirectory()) return -1;
                if(f2.isDirectory()) return  1;
                return f1.getName().toLowerCase().compareTo(f2.getName().toLowerCase());
            };
            sortTreeItem(t.getRoot(), comparator);
            return true;
        });


        // ROW Factory, can be used for applying backgrounds
        treeView.setRowFactory(param -> {
            TreeTableRow<GitFile> row = new TreeTableRow<GitFile>(){
                @Override
                protected void updateItem(GitFile item, boolean empty) {
                    super.updateItem(item, empty);
                }
            };

            row.setOnMouseClicked(event -> {
                if(row.getTreeItem() == null) return;
                GitFile itemClicked = row.getTreeItem().getValue();
                if(itemClicked != null && onItemClickCallback != null && itemClicked.isFile() && event.getClickCount() == 2 && event.getButton() == MouseButton.PRIMARY)
                    onItemClickCallback.onItemClick(itemClicked);
                if(itemClicked != null && onItemContextMenuOpen != null && event.getButton() == MouseButton.SECONDARY)
                    onItemContextMenuOpen.onContextMenu(event.getScreenX(), event.getScreenY());
            });

            return row;
        });

        // column name
        TreeTableColumn<GitFile, GitFile> nameColumn = new TreeTableColumn<>("Filename");
        nameColumn.setPrefWidth(250);
        nameColumn.setMinWidth(150);
        nameColumn.setCellValueFactory(entry -> {
            GitFile item = entry.getValue().getValue();
            return new ReadOnlyObjectWrapper<>(item);
        });
        nameColumn.setCellFactory(value -> {
            return new TreeTableCell<GitFile, GitFile>(){
                @Override
                protected void updateItem(GitFile item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty){
                        setText(null);
                        setGraphic(null);
                    } else {
                        setAlignment(Pos.CENTER_LEFT);
                        setTextFill(item.getStatus().getColor());
                        setText(item.getName());
                        String icon = item.isFile() ? "icons/file-outline-24.png" : "icons/folder-open-24.png";
                        setGraphic(new ImageView(new Image(icon)));
                    }
                }
            };
        });
        nameColumn.setSortable(false);
        nameColumn.setResizable(true);

        // date column
        TreeTableColumn<GitFile, GitFile> dateColumn = new TreeTableColumn<>("Last modified");
        dateColumn.setPrefWidth(150);
        dateColumn.setCellValueFactory(entry -> {
            GitFile item = entry.getValue().getValue();
            return new ReadOnlyObjectWrapper<>(item);
        });
        dateColumn.setCellFactory(value -> {
            return new TreeTableCell<GitFile, GitFile>(){
                @Override
                protected void updateItem(GitFile item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty){
                        setText(null);
                    }else {
                        setAlignment(Pos.CENTER);
                        setTextFill(item.getStatus().getColor());
                        long date = item.getFile().lastModified();
                        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
                        setText(sdf.format(date));
                    }
                }
            };
        });
        dateColumn.setSortable(false);
        dateColumn.setResizable(false);

        // state column
        TreeTableColumn<GitFile, GitFile> stateColumn = new TreeTableColumn<>("Status");
        stateColumn.setPrefWidth(100);
        stateColumn.setCellValueFactory(entry -> {
            GitFile item = entry.getValue().getValue();
            return new ReadOnlyObjectWrapper<>(item);
        });
        stateColumn.setCellFactory(value -> {
            return new TreeTableCell<GitFile, GitFile>(){
                @Override
                protected void updateItem(GitFile item, boolean empty) {
                    super.updateItem(item, empty);
                    if(empty) setText(null);
                    else {
                        setAlignment(Pos.CENTER);
                        setText(item.getStatus().toString());
                        setTextFill(item.getStatus().getColor());
                    }
                }
            };
        });
        stateColumn.setSortable(false);
        stateColumn.setResizable(false);


        treeView.getColumns().add(nameColumn);
        treeView.getColumns().add(dateColumn);
        treeView.getColumns().add(stateColumn);
    }

    /*
        PUBLIC METHODS
     */
    public void reload(){
        if(treeView.getRoot() != null) treeView.getRoot().getChildren().clear();
        treeView.setRoot(getNodesForDirectory(git.getProjectPath()));
        treeView.sort();
    }

    public Set<GitFile> getSelectedItems(){
        List<TreeItem<GitFile>> selected = treeView.getSelectionModel().getSelectedItems();
        return Trees.flatten(selected);
    }

    public Set<String> getSelectedFilepatterns(){
        return treeView.getSelectionModel().getSelectedItems().stream().map(treeItem -> git.getFilepattern(treeItem.getValue())).collect(Collectors.toSet());
    }

    /*
        PRIVATE METHODS
     */
    private <T> void sortTreeItem(TreeItem<T> item, Comparator<TreeItem<T>> comparator){
        if(item.isLeaf()) return;
        FXCollections.sort(item.getChildren(), comparator);
        item.getChildren().forEach(t -> sortTreeItem(t, comparator));
    }

    private void addSet(DirectoryNode<GitFile> root, GitFile.Status status, Path basePath, Set<String> paths){
        for(String str : paths){
            Path path = basePath.resolve(str);
            root.addElement(str, new GitFile(path.toFile(), status));
        }
    }

    private TreeItem<GitFile> getNodesForDirectory(File dir){
        try {
            DirectoryNode<GitFile> root = new DirectoryNode<GitFile>("", new GitFile(dir), null);

            Path basePath = dir.toPath();

            Status status = git.getGit().status().call();
            addSet(root, GitFile.Status.CHANGED, basePath, status.getModified()); //changed,  but unstaged
            addSet(root, GitFile.Status.CHANGED, basePath, status.getChanged()); //changed, staged
            addSet(root, GitFile.Status.ADDED, basePath, status.getAdded());
            addSet(root, GitFile.Status.UNTRACKED, basePath, status.getRemoved());
            addSet(root, GitFile.Status.UNTRACKED, basePath, status.getUntracked());
            addSet(root, GitFile.Status.CURRENT, basePath, GitUtils.getHeadFiles(git.getRepository()));

            Trees.normalizeTree(basePath, root);
            return Trees.convertToTreeItem(root);
        } catch (GitAPIException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setOnItemsSelected(OnItemsSelectCallback onItemsSelectCallback) {
        this.onItemsSelectCallback = onItemsSelectCallback;
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public void setOnItemContextMenuOpen(OnItemContextMenuOpen onItemContextMenuOpen) {
        this.onItemContextMenuOpen = onItemContextMenuOpen;
    }
}
