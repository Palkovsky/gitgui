package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import org.eclipse.jgit.api.ResetCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import pl.dawidmacek.datastructs.GitFile;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.handlers.FileContextMenuHandler;
import pl.dawidmacek.utils.RecursiveDirectoryWatcher;
import pl.dawidmacek.utils.StageSpawner;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Set;

public class ProjectController extends AbstractController implements Initializable {

    private GitWrapper git;

    //Views
    @FXML
    private AnchorPane leftPane;
    @FXML
    private AnchorPane centerPane;
    @FXML
    private AnchorPane bottomPane;
    @FXML
    private MenuItem menuItemProjectReload;
    @FXML
    private MenuItem menuItemOpen;
    @FXML
    private MenuItem menuItemClose;
    @FXML
    private MenuItem menuItemSettings;
    @FXML
    private MenuItem menuItemCommit;
    @FXML
    private MenuItem menuItemCommitHistory;
    @FXML
    private MenuItem menuItemBranches;
    @FXML
    private MenuItem menuItemMerge;
    @FXML
    private MenuItem menuItemHeadMixedReset;
    @FXML
    private MenuItem menuItemHeadHardReset;
    @FXML
    private MenuItem menuItemAbout;

    //Controllers
    private FileListingController fileListingController;
    private StatusController statusController;
    private StagedInfoController stagedInfoController;

    public ProjectController(GitWrapper git) {
        this.git = git;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        setUpFileListingPane();
        setUpStatusPane();
        setUpStagedInfoPane();
        setUpMenuItems();
        setupDirectoryWatcher();
    }

    private void setUpMenuItems() {
        menuItemProjectReload.setOnAction(e -> {
            reload();
        });

        menuItemOpen.setOnAction(e -> {
            GitWrapper newGit = StageSpawner.spawnOpenProjectDialog(getStage());
            if (newGit != null) StageSpawner.spawnProjectStage(newGit);
        });

        menuItemClose.setOnAction(e -> getStage().close());

        menuItemCommit.setOnAction(e -> {
            String dialogTitle = git.getProjectPath() + " - Commit history[" + git.getCurrentBranch() + "]";
            if (stagedInfoController.getStaged().isEmpty()) {
                StageSpawner.spawnMessageDialog(dialogTitle, "Nothing staged, dummy.", getStage());
                return;
            }

            CommitDialogController commitDialogController = new CommitDialogController(git, stagedInfoController.getStaged());
            commitDialogController.setOnCommitListener(commit -> reload());
            StageSpawner.spawnModalDialog(
                    StageSpawner.loadFXMLWithController(StageSpawner.PERFORM_COMMIT_DIALOG, commitDialogController),
                    dialogTitle, getStage());
        });

        menuItemCommitHistory.setOnAction(e -> {
            StageSpawner.spawnModalDialog(
                    StageSpawner.loadFXMLWithController(StageSpawner.COMMIT_HISTORY_DIALOG, new CommitHistoryController(git)),
                    git.getProjectPath() + " - Commit history[" + git.getCurrentBranch() + "]", getStage());
        });

        menuItemMerge.setOnAction(e -> StageSpawner.spawnModalDialog(
                StageSpawner.loadFXMLWithController(StageSpawner.MERGE_DIALOG, new MergeDialogController(git)),
                git.getProjectPath() + " - Merge[" + git.getCurrentBranch() + "]", getStage()));

        menuItemHeadHardReset.setOnAction(e -> {
            try {
                GitUtils.unstageToHead(git.getGit(), ResetCommand.ResetType.HARD);
                reload();
            } catch (GitAPIException e1) {
                StageSpawner.spawnErrorDialog(e1, getStage());
            }
        });

        menuItemHeadMixedReset.setOnAction(e -> {
            try {
                GitUtils.unstageToHead(git.getGit(), ResetCommand.ResetType.MIXED);
                reload();
            } catch (GitAPIException e1) {
                StageSpawner.spawnErrorDialog(e1, getStage());
            }
        });

        menuItemSettings.setOnAction(e -> StageSpawner.spawnModalDialog(StageSpawner.SETTINGS, "Settings", getStage()));
        menuItemAbout.setOnAction(e -> StageSpawner.spawnMessageDialog("About", "Lubię kotlety schabowe.", getStage()));
    }

    private void setUpFileListingPane() {
        fileListingController = new FileListingController(git);

        // keep track of selected items
        fileListingController.setOnItemsSelected(gitFiles -> {
            if (gitFiles.size() == 0) statusController.setStatusMessage("Ready!");
            else if (gitFiles.size() == 1) statusController.setStatusMessage(gitFiles.iterator().next().getName());
            else
                statusController.setStatusMessage(gitFiles.size() + " files selected (" + fileListingController.getSelectedFilepatterns().size() + ").");
        });

        fileListingController.setOnItemClickCallback(gitFile -> gitFile.open(getStage()));

        fileListingController.setOnItemContextMenuOpen((double x, double y) -> {
            FileContextMenuHandler contextMenuHandler = new FileContextMenuHandler();
            Set<GitFile> files = fileListingController.getSelectedItems();
            Set<String> filepatterns = fileListingController.getSelectedFilepatterns();
            filepatterns.forEach(p -> System.out.println(p));
            contextMenuHandler.handle(this, files, filepatterns).show(getStage(), x, y);
        });

        // We're initializing controllers for panes
        centerPane.getChildren().add(
                StageSpawner.loadFXMLWithController(StageSpawner.FILE_LISTING, fileListingController));
    }

    private void setUpStatusPane() {
        statusController = new StatusController(git);
        statusController.setOnStateChangeListener(this::reload);
        statusController.setOnCommitHistoryRequestListener(() -> StageSpawner.spawnModalDialog(
                StageSpawner.loadFXMLWithController(StageSpawner.COMMIT_HISTORY_DIALOG, new CommitHistoryController(git)),
                git.getProjectPath() + " - Commit history[" + git.getCurrentBranch() + "]", getStage()));
        bottomPane.setMaxHeight(20);
        bottomPane.getChildren().add(
                StageSpawner.loadFXMLWithController(StageSpawner.PROJECT_STATUS, statusController));
    }

    private void setUpStagedInfoPane() {
        stagedInfoController = new StagedInfoController(git);
        stagedInfoController.setOnStateChangeListener(this::reload);
        leftPane.getChildren().add(StageSpawner.loadFXMLWithController(StageSpawner.PROJECT_STAGED_INFO, stagedInfoController));
    }

    private void setupDirectoryWatcher() {
        RecursiveDirectoryWatcher watcher = RecursiveDirectoryWatcher.build(git.getProjectPath().toPath(),
                (e, p) -> {
                    System.out.println(e + " - " + p);
                    reload();
                });
        Thread watcherThread = new Thread(watcher);
        watcherThread.setDaemon(true);
        watcherThread.start();
    }

    public void reload() {
        statusController.reload();
        fileListingController.reload();
        stagedInfoController.reload();
    }

    public FileListingController getFileListingController() {
        return fileListingController;
    }

    public StatusController getStatusController() {
        return statusController;
    }

    public StagedInfoController getStagedInfoController() {
        return stagedInfoController;
    }

    public GitWrapper getGit() {
        return git;
    }
}
