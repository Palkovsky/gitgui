package pl.dawidmacek.controllers;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.revwalk.RevCommit;
import pl.dawidmacek.datastructs.GitFile;
import pl.dawidmacek.git.GitUtils;
import pl.dawidmacek.git.GitWrapper;
import pl.dawidmacek.utils.NoSelectionModel;
import pl.dawidmacek.utils.StageSpawner;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

public class CommitDialogController extends AbstractController implements Initializable {

    public interface Callback{
        void onCommit(RevCommit commit);
    }

    @FXML
    private ListView<GitFile> stagedListView;
    @FXML
    private TextArea commitMessageTextArea;
    @FXML
    private Button backBtn;
    @FXML
    private Button commitBtn;

    private GitWrapper git;
    private List<GitFile> staged;
    private Callback callback;

    public CommitDialogController(GitWrapper git, List<GitFile> staged) {
        this.git = git;
        this.staged = staged;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backBtn.setOnMouseClicked(e -> {
            getStage().close();
        });

        commitBtn.setOnMouseClicked(e -> {
            performCommit();
        });

        commitMessageTextArea.textProperty().addListener((observable, oldValue, newValue) -> {
            commitMessageTextArea.setBorder(null);
        });

        stagedListView.setCellFactory(value -> new ListCell<GitFile>(){
            @Override
            protected void updateItem(GitFile item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? null : git.getFilepattern(item));
                setTextFill(empty ? null : item.getStatus().getColor());
            }
        });
        stagedListView.setSelectionModel(new NoSelectionModel<>());
        stagedListView.setItems((ObservableList<GitFile>) staged);
    }

    private void performCommit(){
        String message = commitMessageTextArea.getText();
        if(message.trim().equals("")) {
            commitMessageTextArea.setBorder(new Border(new BorderStroke(Color.RED,
                    BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
            commitMessageTextArea.requestFocus();
            return;
        }
        try {
            RevCommit commit = GitUtils.commit(git.getGit(), message);
            if(callback != null) callback.onCommit(commit);
            getStage().close();
            StageSpawner.spawnMessageDialog("Success!", commit.getName() + ": " +  commit.getFullMessage(), getStage());
        } catch (GitAPIException e) {
            StageSpawner.spawnErrorDialog(e, getStage());
        }

    }

    public void setOnCommitListener(Callback callback) {
        this.callback = callback;
    }
}
