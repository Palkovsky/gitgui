package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

public class NewBranchDialogController extends AbstractController implements Initializable {

    public interface OnSubmitListener {
        void onSubmit(String branchName, boolean checkout);
    }

    private static final int MAX_TEXT_LENGTH = 15;

    @FXML
    private TextField branchNameTextField;
    @FXML
    private CheckBox checkoutCheckbox;
    @FXML
    private Button backBtn;
    @FXML
    private Button createBtn;

    private OnSubmitListener callback;

    public NewBranchDialogController(OnSubmitListener callback) {
        this.callback = callback;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        backBtn.setOnMouseClicked(e -> getStage().close());

        branchNameTextField.lengthProperty().addListener(((observable, oldValue, newValue) -> {
            if (branchNameTextField.getText().length() >= MAX_TEXT_LENGTH)
                branchNameTextField.setText(branchNameTextField.getText().substring(0, MAX_TEXT_LENGTH));
        }));
        branchNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            branchNameTextField.setBorder(null);
        });

        createBtn.setOnMouseClicked(e -> {
            if (branchNameTextField.getText().isEmpty()) {
                branchNameTextField.setBorder(new Border(new BorderStroke(Color.RED,
                        BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
                branchNameTextField.requestFocus();
                return;
            }
            callback.onSubmit(branchNameTextField.getText(), checkoutCheckbox.isSelected());
            getStage().close();
        });
    }
}
