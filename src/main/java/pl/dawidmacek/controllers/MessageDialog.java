package pl.dawidmacek.controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ResourceBundle;

public final class MessageDialog extends AboutController implements Initializable {

    @FXML
    private Label messageLabel;
    @FXML
    private Button okBtn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        okBtn.requestFocus();
        okBtn.setOnMouseClicked(e -> getStage().close());
    }

    public void setMessage(String message) {
        if(getLabel() != null) getLabel().setText(message);
    }

    public Label getLabel() {
        return messageLabel;
    }
}
